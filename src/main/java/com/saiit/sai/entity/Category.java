package com.saiit.sai.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="category_tab")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Category {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	@Column(name="cat_name_col")
	private String name;
	@Column(name="col_alias_col")
	
	private String alias;
	@Column(name="col_ctype_col")
	private String ctype;
	@Column(name="col_status_col")
	private String status;
	@Column(name="col_note_col")
	private String note;
	
	//---- Association Mapping -----
	@ManyToOne
	@JoinColumn(name="cat_type_fk_col")
	private CategoryType categoryType;
	
}
