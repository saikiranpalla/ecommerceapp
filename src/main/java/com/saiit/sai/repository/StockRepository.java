package com.saiit.sai.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.saiit.sai.entity.Stock;

public interface StockRepository extends JpaRepository<Stock, Long> {
	
	//From selected Product find Stock exist or not by using Stock.id
	//To find selected product existed already in stock table or not?
	@Query("SELECT s.id FROM Stock s INNER JOIN s.product as p WHERE p.id=:productId")
	Long getStockIdByProduct(Long productId);
	
	//Updating the Stock
	//Update existed product qoh
	@Modifying
	@Query("UPDATE Stock SET qoh=qoh+:count WHERE id=:id")
	void updateStock(Long id,Long count);

}
