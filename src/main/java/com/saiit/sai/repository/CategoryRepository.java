package com.saiit.sai.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.saiit.sai.entity.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {
	
	//based on status it is showing id and name
	@Query("SELECT id, name FROM Category WHERE status=:status")
	List<Object[]> getCategoryIdAndName(String status);
	
	//Fetch Categories based on CategoryType
	//word inner is optional for joining the query
	@Query("SELECT c FROM Category c JOIN c.categoryType as categoryType WHERE categoryType.id=:id")
	List<Category> getCategoryByCategoryType(Long id);

}
