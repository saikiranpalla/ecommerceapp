package com.saiit.sai.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.saiit.sai.entity.Coupon;

public interface CouponRepository extends JpaRepository<Coupon, Long> {

}
