package com.saiit.sai.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.saiit.sai.entity.User;

public interface UserRepository extends JpaRepository<User,Long> {

	//find the user
	Optional<User> findByEmail(String email);
	
	//find the user by mobile number
	Optional<User> findByContact(String contact);
}
