package com.saiit.sai.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.saiit.sai.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
