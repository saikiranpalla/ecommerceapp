package com.saiit.sai.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.saiit.sai.entity.CategoryType;

public interface CategoryTypeRepository extends JpaRepository<CategoryType, Long> {

	@Query("select id,name from CategoryType")
	List<Object[]> getCategoryTypeIdAndName();
}
