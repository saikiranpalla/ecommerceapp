package com.saiit.sai.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.saiit.sai.entity.Shipping;

public interface ShippingRepository extends JpaRepository<Shipping, Long> {

}
