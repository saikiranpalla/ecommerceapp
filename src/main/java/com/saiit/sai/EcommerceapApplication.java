package com.saiit.sai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcommerceapApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceapApplication.class, args);
	}

}
