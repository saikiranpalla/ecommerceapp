package com.saiit.sai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.saiit.sai.entity.Customer;
import com.saiit.sai.service.ICustomerService;

@RequestMapping("/customer")
@Controller
public class CustomerController {
	@Autowired
	private ICustomerService service;
	
	//show register page
	@GetMapping("/register")
	public String showRegister()
	{
		
		return "CustomerRegister";
	}
	
	//save customer
	/***
	 * 
	 */
	@PostMapping("/save")
	public String saveCustomer(@ModelAttribute Customer customer,Model model)
	{
		Long id=service.saveCutomer(customer);
		model.addAttribute("message","Customer is created with id:"+id);
		return "CustomerRegister";
	}
	
	//get all customers
	@GetMapping("/all")
	public String getAllCustomers(Model model)
	{
		List<Customer> list=service.getAllCustomers();
		model.addAttribute("list",list);
		return "CustomerData";
	}
	
}
