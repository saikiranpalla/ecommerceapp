package com.saiit.sai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.saiit.sai.entity.Coupon;
import com.saiit.sai.exception.CouponNotFoundException;
import com.saiit.sai.service.ICouponService;

@Controller
@RequestMapping("/coupon")
public class CouponController {
	
	@Autowired
	private ICouponService service;
	
	//show Register Page
	@GetMapping("/register")
	public String showRegister()
	{
		return "CouponRegister";
	}
	//save on submit
	/***
	 * On Form submit, read data as object by using @ModelAttriute classname objname 
	 * call service layer with object and Id back
	 * create message as String
	 * Use Model memory to send message back to UI
	 * Return back to CouponRegister.html
	 */
	@PostMapping("/save")
	public String saveCoupon(@ModelAttribute Coupon coupon,Model model) 
	{
		Long id=service.saveCoupon(coupon);
		model.addAttribute("message","coupon created with Id:"+id);
		return "CouponRegister";
	}
	
	//display data
	/***
	 * Fetch all data from Db by using service
	 * store it as list type
	 * send data to ui using model
	 * return back to CouponData.html
	 * 
	 * Get the message from @RequestParam(value="message",required = false)String message
	 * and append message to CouponData
	 */
	@GetMapping("/all")
	public String getAllCoupons(Model model,@RequestParam(value="message",required = false)String message)
	{
		List<Coupon> list= service.getAllCoupons();
		model.addAttribute("list",list);
		model.addAttribute("message",message);
		return "CouponData";
	}
	
	//delete by id
	/***
	 * Read Id from Request URL by using @RequestParam
	 * call service for delete
	 * delete the row based on id
	 * get latest data
	 * create success message and send message by using RedirectAttributes to redirect to all
	 * handle the exception if record is not there
	 * return back All[CouponData]
	 */
	@GetMapping("/delete")
	public String deleteCoupon(@RequestParam Long id,RedirectAttributes attributes)
	{
		try {
			service.deleteCoupon(id);
			attributes.addAttribute("message","Coupon is deleted with Id:"+id);
		} catch(CouponNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message",e.getMessage());
		}
		return "redirect:all";
	}
	//show edit
	/***
	 * Read Id from url by using @RequestParam
	 * load the DB row by using findById  
	 * send object to ui by using Model
	 * use Thymleaf form to read object data and fill it
	 * use try and catch blocks 
	 
	 Fetch data into edit page
	 *End user clicks on link or may enter data manually
	 *If entered Id is present in Db 
	 * > Load row as object
	 * >send data to edit page
	 * >Fill the form
	 * 
	 * Else
	 * >Redirect to all(Data page)
	 * >show error message(Not Found)
	 */
	@GetMapping("/edit")
	public String editCoupon(@RequestParam Long id,Model model)
	{
		String page=null;
		try {
			Coupon cu=service.getOneCoupon(id);
			model.addAttribute("coupon",cu);
			page="CouponEdit";
		} catch (CouponNotFoundException e) {
			e.printStackTrace();
			model.addAttribute("message",e.getMessage());
			page="redirect:all";
		}
		return page;
	}
	
	//do update
	/***
	 * Take object from edit page by using @ModelAttribute classname objname
	 * call service for save/update
	 * send message to ui by using RedirectAttributes
	 * after updating redirect to all
	 */
	@PostMapping("/update")
	public String updateCoupon(@ModelAttribute Coupon coupon,RedirectAttributes attributes)
	{
		service.saveCoupon(coupon);
		attributes.addAttribute("message","Coupon updated");
		return "redirect:all";
	}
}
