package com.saiit.sai.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.saiit.sai.constants.UserRole;
import com.saiit.sai.entity.User;
import com.saiit.sai.service.IUserService;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private IUserService service;
	
	//show register page
	@GetMapping("/register")
	public String showRegister()
	{
		return "UserRegister";
	}
	
	//save user
	/***
	 * On Form submit get the object by using @ModelAttribute clname objname
	 * call the service for saving the object into DB
	 * after saving the object read the id back
	 * send the message back to UI by using Model memory
	 */
	@PostMapping("/save")
	public String saveUser(@ModelAttribute User user,Model model)
	{
		Long id=service.saveUser(user);
		model.addAttribute("message","User is created with id:"+id);
		return "UserRegister";
	}
	
	//get all users
	/***
	 * Get all the users from DB
	 * Store it as list type
	 * send to UI by using Model memory
	 * 
	 */
	@GetMapping("/all")
	public String getAllUsers(Model model)
	{
		List<User> list=service.getAllUsers();
		model.addAttribute("list",list);
		return "UserData";
	}
	
	
	//validate email
	@ResponseBody
	@GetMapping("/validateMail")
	public String validateEmail(
			@RequestParam("email")String email)
	{
		String message="";
		if(service.findByEmail(email).isPresent())
		{
			message=email+" already exist";
		}
		return message;
	}
	
	//validate Mobile
	@ResponseBody
	@GetMapping("/validatemobile")
	public String validateMobile(@RequestParam("contact")String contact)
	{	String message="";
		if(service.findByContact(contact).isPresent())
		{
			message =contact +" already exist";
		}
		return message;
	}
	
	@GetMapping("/profile")
	public String showProfile() {
		return "UserProfile";
	}

	
	@GetMapping("/setup")
	public String setupUser(Authentication authentication,HttpSession httpSession)
	{
		//fetch current user oject
		String displayName=service.findByEmail(authentication.getName())
				.get().getDisplayName();
		httpSession.setAttribute("displayName", displayName);
		
		String page=null;
		@SuppressWarnings("unchecked")
		String role = ((List<GrantedAuthority>)authentication.getAuthorities()).get(0).getAuthority();
		
		if(role.equals(UserRole.ADMIN.name())) {
			page = "/user/all";
		} else if(role.equals(UserRole.EMPLOYEE.name())) {
			page = "/brand/all";
		} else if(role.equals(UserRole.SALES.name())) {
			page = "/product/all";
		} else  if(role.equals(UserRole.CUSTOMER.name())) {
			page = "/product/search";
		}
		return "redirect:"+page;
		
	}
	
	@GetMapping("/login")
	public String showLoginPage() {
		return "UserLogin";
	}

}
