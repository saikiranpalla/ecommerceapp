package com.saiit.sai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.saiit.sai.entity.Brand;
import com.saiit.sai.exception.BrandNotFoundException;
import com.saiit.sai.service.IBrandService;

@Controller
@RequestMapping("/brand")
public class BrandController {

	@Autowired
	private IBrandService service;

	// show Register Page

	@GetMapping("/register")
	public String showRegister() {
		return "BrandRegister";
	}

	// save on submit
	/***
	 * On Form submit,read data as object by using @ModelAttriute classname objname
	 * call service layer with object,read Id back create message as String use
	 * Model memory,to send message back to UI Return back to BrandRegister.html
	 */

	@PostMapping("/save")
	public String saveBrand(@ModelAttribute Brand brand, Model model) {
		Long id = service.saveBrand(brand);
		model.addAttribute("message", "Brand created with Id:" + id);
		return "BrandRegister";
	}

	// display data
	/***
	 * Fetch All Data from Db Using service store it as list type send data to ui
	 * using model return back to BrandData.html
	 * 
	 * Get the message from @RequestParam(value="message",required = false)String
	 * message and append message to CategoryTypeData
	 */

	@GetMapping("/all") 
	public String getAllBrands(Model model,
			@RequestParam(value="message",required=false)String message) {
		List<Brand> list=service.getAllBrands(); 
		model.addAttribute("list",list);
		model.addAttribute("message",message);
		return "BrandData"; 
	}

	//delete by id
	/***
	  Read Id from Request URL by using @RequestParam call service for delete
	  delete the row based on id get latest data create success message and send
	  message by using RedirectAttributes to redirect to all handle the exception
	  if record is not there return back to BrandData.html
	 ***/  
	@GetMapping("/delete") 
	public String deleteBrand(@RequestParam Long
			id,RedirectAttributes attributes) 
	{
		try { 
			service.deleteBrand(id);
			attributes.addAttribute("message","Brand deleted with Id:"+id); 
		} catch(BrandNotFoundException e) 
		{
			e.printStackTrace();
			attributes.addAttribute(e.getMessage());
		} 
		return "redirect:all"; 
	}

	//show edit
	/***
	 * Read Id from Url by using @RequestParam load the Db row by using findById
	 * send Object to ui by using Model use Thymleaf form to read object data and
	 * fill form
	 * 
	 * use try and catch blocks Fetch Data into edit page -------------------------
	 * End user clicks on link or may enter data manually If entered Id is present
	 * in Db >Load row as object >send data to edit page >Fill the form
	 * 
	 * Else >Redirect to all(Data page) >show error message(Not Found)
	 */
	@GetMapping("/edit") 
	public String editBrand(@RequestParam Long id,Model model) 
	{
		String page=null; 
		try { 
			Brand br=service.getOneBrand(id);
			model.addAttribute("brand",br); 
			page="BrandEdit"; 
		} catch(BrandNotFoundException e) 
		{
			e.printStackTrace();
			model.addAttribute("message",e.getMessage()); 
			page="redirect:all"; 
		}
		return page; 
	}

	//do update
	/***
	 * Take object data from edit page by using @ModelAttribute call service and
	 * append the updated message by using RedirectAttributes after updating
	 * redirect back to all
	 * 
	 */
	@PostMapping("/update")
	public String updateBrand(@ModelAttribute Brand brand,RedirectAttributes attributes)
	{ 
		service.updateBrand(brand);
		attributes.addAttribute("message","Brand updated"); 
		return "redirect:all"; 
	}


}
