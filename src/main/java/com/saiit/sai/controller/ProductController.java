package com.saiit.sai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.saiit.sai.entity.Product;
import com.saiit.sai.exception.ProductNotFoundException;
import com.saiit.sai.service.IBrandService;
import com.saiit.sai.service.ICategoryService;
import com.saiit.sai.service.IProductService;

@Controller
@RequestMapping("/product")
public class ProductController {
	@Autowired
	private IProductService service;
	@Autowired
	private ICategoryService categoryService;
	@Autowired
	private IBrandService brandService;
	
	private void commonUi(Model model)
	{
		model.addAttribute("categories",categoryService.getCategoryIdAndName("ACTIVE"));
		model.addAttribute("brands",brandService.getBrandIdAndName());
	}
	//SHOW REGISTER PAGE
	@GetMapping("/register")
	public String showRegister(Model model)
	{
		commonUi(model);
		return "ProductRegister";
	}
	
	//save product
	/***
	 * On Form submit read the object by using @ModelAttriute classname objname
	 * call the service layer, save the data and get the ID back
	 * create message as string
	 * Use Model memory to send the message back to UI
	 * return back to ProductRegister.html
	 */
	@PostMapping("/save")
	public String saveProduct(@ModelAttribute Product product,Model model)
	{
		Long id=service.saveProduct(product);
		String message="Product created with id:"+id;
		model.addAttribute("message",message);
		commonUi(model);
		return "ProductRegister";
	}
	
	//list all products
	/***
	 * Fetch all the data from db bby using service
	 * store it as list type
	 * send back the data by using Model memory
	 * return data to ProductData.html
	 * 
	 * Get the message after deleting the Product by using @RequestParam(value="message",required = false)String message
	 * and append the message to CouponData
	 */
	@GetMapping("/all")
	public String showAll(Model model,@RequestParam(value="message",required = false)String message)
	{
		List<Product> list=service.getAllProducts();
		model.addAttribute("list",list);
		model.addAttribute("message",message);
		return "ProductData";
	}
	//delete product by id
	/***
	 * Get the id from request url by using @RequestParam 
	 * call the service for delete
	 * delete the row based in id
	 * create success/failure message and msg by using RedirectAttriutes to redirect to all 
	 * handle the exception if record is not there
	 * return back to all
	 */
	@GetMapping("/delete")
	public String deleteProduct(@RequestParam Long id,RedirectAttributes attributes)
	{
		try {
			service.deleteProduct(id);
			attributes.addAttribute("message","Product is deleted with id:"+id);
		} catch (ProductNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message",e.getMessage());
		}
		return "redirect:all";
	}
	
	//show edit
	/***
	 * Read Id from URL by using @RequestParam
	 * load the db row by using findById
	 * send the object to ui by using Model 
	 * use Thymleaf form to read data and fill it [ProductEdit]
	 * use try and catch blocks
	 * 
	 Fetch data into edit page
	 * If end user clicks on link or may enter data manually
	 * If entered Id is Present in db 
	 * > Load the row as object 
	 * >send data to edit page
	 * >Fill the form
	 * 
	 *  Else 
	 *  >Redirect to all page[ProductData]
	 *  >Show the error message
	 */
	@GetMapping("/edit")
	public String editProduct(@RequestParam Long id,Model model)
	{	
		String page=null;
		try {
			Product product=service.getOneProduct(id);
			model.addAttribute("product",product);
			page="ProductEdit";
		} catch (ProductNotFoundException e) {
			e.printStackTrace();
			model.addAttribute("message",e.getMessage());
			page="redirect:all";
		}
		return page;
	}
	
	//do update
	/***
	 * Take the object from edit page by using @ModelAttribute claname objname
	 * call the service for save/update
	 * send message to ui b using RedirectAttributes
	 * after updating redirect:all
	 * 
	 */
	@PostMapping("/update")
	public String updateProduct(@ModelAttribute Product product,RedirectAttributes attributes)
	{
		service.updateProduct(product);
		attributes.addAttribute("message","Product updated");
		return "redirect:all";
	}

}
