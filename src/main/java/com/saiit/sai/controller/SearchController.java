package com.saiit.sai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.saiit.sai.entity.Category;
import com.saiit.sai.entity.CategoryType;
import com.saiit.sai.entity.Product;
import com.saiit.sai.service.IBrandService;
import com.saiit.sai.service.ICategoryService;
import com.saiit.sai.service.ICategoryTypeService;
import com.saiit.sai.service.IProductService;

@Controller
@RequestMapping({"/search","/"})
public class SearchController {
	
	@Autowired
	private IBrandService brandService;
	@Autowired
	private IProductService productService;
	
	@Autowired
	private ICategoryTypeService categoryTypeService;
	@Autowired
	private ICategoryService categoryService;
	
	//get Brand Image and Brand Id
	@GetMapping("/")
	public String showBrands(Model model)
	{
		List<Object[]> list=brandService.getBrandIdAndImage();
		model.addAttribute("list",list);
		return "BrandPage";
	}
	
	@GetMapping("/products")
	public String showProductsByBrand(
				@RequestParam("brandId")Long brandId,
				Model model
			)
	{
		List<Object[]> list=productService.getProductsByBrand(brandId);
		model.addAttribute("list",list);
		return "ProductsResult";
	}
	
	//show all categoryTypes
	@GetMapping("/categories")
	public String showAllCategoryTypes(Model model)
	{
		List<CategoryType> list=categoryTypeService.getAllCategoryTypes();
		model.addAttribute("list",list);
		return "CategoryTypesPage";
	}
	
	//show all categories by categoryType
	
	@GetMapping("/getCategories") 
	public String showAllCategoriesByType(@RequestParam Long catTypeId,Model model) 
	{ 
		List<Category> list=categoryService.getCategoryByCategoryType(catTypeId);
		model.addAttribute("list",list);
		return "CategoriesPage"; 
	}
	  
	  //show all Products by category
	  @GetMapping("/categoryProducts") 
	  public String showAllProductsByCategory(@RequestParam Long catId,Model model) 
	  { 
		  List<Object[]> list=productService.getProductsByCategory(catId);
		  model.addAttribute("list",list);
		  return "ProductsResult"; 
	  } 
	 
	  @GetMapping("/byProduct")
	  public String showAllProductsByName(@RequestParam String productname,Model model)
	  {
		  List<Object[]> list=productService.getProductByNameMatching(productname);
		  model.addAttribute("list",list);
		  return "ProductsResult";
	  }
	  
	  @GetMapping("/productViewById")
	  public String viewOneProductById(Long prodId,Model model)
	  {
		  Product pob=productService.getOneProduct(prodId);
		  model.addAttribute("pob",pob);
		  return "ProductViewPage";
	  }
}
