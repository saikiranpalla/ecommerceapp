package com.saiit.sai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.saiit.sai.entity.Category;
import com.saiit.sai.exception.CategoryNotFoundException;
import com.saiit.sai.service.ICategoryService;
import com.saiit.sai.service.ICategoryTypeService;

@Controller
@RequestMapping("/category")
public class CategoryController {
	
	@Autowired
	private ICategoryService service;
	
	@Autowired
	private ICategoryTypeService categoryTypeService;
	
	private void commonUi(Model model)
	{
		model.addAttribute("categoryTypes",categoryTypeService.getCategoryTypeIdAndName());
	}
	
	@GetMapping("/register")
	public String showRegister(Model model)
	{
		commonUi(model);
		return "CategoryRegister";
	}
	
	/***
	 * On Form submit,Read data as object using @ModelAttribute
	 * call Service layer with object, read ID back
	 * Create message as String 
	 * use Model memory,to send message to UI
	 * Return back to CategoryRegister.html
	 */
	@PostMapping("/save")
	public String saveCategory(
			@ModelAttribute Category category,Model model
			)
	{
		Long id=service.saveCategory(category);
		model.addAttribute("message","category created with Id:"+id);
		commonUi(model);
		return "CategoryRegister";
	}
	
	/***
	 * Fetch Data from DB using Service
	 * send data to UI using Model
	 * Return to CategoryData.html
	 * 
	 * Get the message from @RequestParam(value="message",required = false)String message
	 * and append message to CategoryTypeData
	 */
	@GetMapping("/all")
	public String getAllCategorys(Model model, @RequestParam(value = "message", required = false) String message)
	{
		List<Category> list=service.getAllCategories();
		model.addAttribute("list",list);
		model.addAttribute("message",message);
		return "CategoryData";
	}
	
	/***
	 * Read id from Request URL @RequestParam
	 * call service for delete
	 * delete the row based on id
	 * get latest data
	 * create success message and send message by using RedirectAttributes to redirect to all
	 * handle the exception if record is not there
	 * return back to CategoryData.html
	 */
	
	@GetMapping("/delete")
	public String deleteCategory(
			@RequestParam Long id,RedirectAttributes attributes
			)
	{
		try {
		//call service
		service.deleteCategory(id);
		attributes.addAttribute("message","category deleted with Id:"+id);
		}catch (CategoryNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message",e.getMessage());
		}
		return "redirect:all";
	}
	
	/***
	 * show edit
	 * read Id from Request Param
	 * load DB row by using findById and take it as class type
	 * send object to UI using Model
	 * use thymeleaf Form reads data from Object and fill it
	 *
	 Fetch Data into Edit Page
	 * End user clicks on link,may enter ID manually
	 * If entered ID is Present in DB
	 * 	>Load row as object
	 * 	>Send data to edit page
	 *  >Fill in form 
	 * 
	 *  Else
	 *    >Redirect to all(Data page)
	 *    >	Show Error message(Not Found)
	*/
	@GetMapping("/edit")
	public String editCategory(@RequestParam Long id,Model model,RedirectAttributes attributes)
	{
		String page=null;
		try {
				//load object from DB
				Category ob=service.getOneCategory(id);
				//send object to UI
				model.addAttribute("category",ob);
				page="CategoryEdit";
				commonUi(model);
		} catch (CategoryNotFoundException e) {
				e.printStackTrace();
				attributes.addAttribute("message",e.getMessage());
				page="redirect:all";
		}
		return page;
	}
	
	/***
	 * Do update
	 * take object data from edit page  by using @ModelAttribute classname objname
	 * call service
	 * and append the updated message by using RedirectAttributes
	 * after updating redirect back to all
	 */
	@PostMapping("/update")
	public String updateCategory(@ModelAttribute Category category,RedirectAttributes attributes)
	{
		service.updateCategory(category);
		attributes.addAttribute("message", "Category updated");
		return "redirect:all"; 
	}
}
