package com.saiit.sai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.saiit.sai.entity.CategoryType;
import com.saiit.sai.exception.CategoryTypeNotFoundException;
import com.saiit.sai.service.ICategoryTypeService;

@Controller
@RequestMapping("/categorytype")
public class CategoryTypeController {

	@Autowired
	private ICategoryTypeService service;
	
	//show Register page
	@GetMapping("/register")
	public String showRegister()
	{
		return "CategoryTypeRegister";
	}
	
	//save on submit
	/***
	 * On Form submit,Read data as object using @ModelAttribute className objectName
	 * call service layer, with object and get Id back
	 * create message as String,
	 * use Model memory to send message to UI
	 * Return back to CategoryTypeRegister.html
	 */
	@PostMapping("/save")
	public String saveCategoryType(@ModelAttribute CategoryType categoryType,Model model)
	{
		Long id=service.saveCategoryType(categoryType);
		model.addAttribute("message","CategoryType created with Id:"+id);
		return "CategoryTypeRegister";
	}
	
	//display data
	/***
	 * Fetch data from DB using Service
	 * send data to UI using Model
	 * Return back to CategoryTypeData.html
	 * 
	 * Get the message from delete api @RequestParam(value="message",required = false)String message
	 * and append message to CategoryTypeData
	 */
	
	@GetMapping("/all")
	public String getAllCategoryTypes(Model model,@RequestParam(value="message",required = false)String message)
	{
		List<CategoryType> list=service.getAllCategoryTypes();
		model.addAttribute("list",list);
		model.addAttribute("message",message);
		return "CategoryTypeData";
	}
	
	//delete by Id
	/***
	 * Read id from Request URL @RequestParam
	 * call service for delete
	 * delete the row based on id
	 * get latest data
	 * create success message and send message by using RedirectAttributes to redirect to all
	 * handle the exception if record is not there
	 * return back to CategoryTypeData.html
	 */
	
	@GetMapping("/delete")
	public String deleteCategoryType(@RequestParam Long id,RedirectAttributes attributes)
	{
		try {
			service.deleteCategoryType(id);
			attributes.addAttribute("message","CategoryType deleted with Id:"+id);
		} catch (CategoryTypeNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message",e.getMessage());	
		}
		return "redirect:all";
	}
	
	//show edit
	/***
	 * Read Id from requestParam
	 * load DB row by using findById
	 * send object to UI by using model
	 * use thymleaf form reads data from object and fill it 
	 * 
	 * 
	 Fetch Data into edit page
	 End user clicks on link,may enter Id manually
	 If entered Id is Present in db
	 Load row as object
	 send data to edit page
	 Fill in form
	 
	 Else
	 Redirect to all(Data page)
	 show error message(Not found)	
	 */
	@GetMapping("/edit")
	public String editCategoryType(@RequestParam Long id,Model model,RedirectAttributes attributes)
	{
		String page=null;
		try {
		CategoryType ob=service.getOneCategoryType(id);
		model.addAttribute("categorytype",ob);
		page="CategoryTypeEdit";
		}
		catch (CategoryTypeNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message",e.getMessage());
			page="redirect:all";
		}
		return page;
	}
	
	//update
	/***
	 * Take Object data from edit page by using @ModelAttribute classname objname
	 * call service
	 * and append the updated message by using RedirectAttributes
	 * after updating redirect back to all
	 */
	@PostMapping("/update")
	public String updateCategoryType(@ModelAttribute CategoryType categoryType,RedirectAttributes attributes)
	{
		service.updateCategoryType(categoryType);
		attributes.addAttribute("message","CategoryType updated");
		return "redirect:all";
	}
	
}
