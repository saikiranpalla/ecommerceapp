package com.saiit.sai.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.saiit.sai.entity.Stock;
import com.saiit.sai.service.IProductService;
import com.saiit.sai.service.IStockService;

@Controller
@RequestMapping("/stock")
public class StockController {
	@Autowired
	private IStockService service;
	
	//For Product dynamic dropdown we need IProductService
	@Autowired
	private IProductService productService;
	
	//Module Integration
	
	private void commonUi(Model model) {
		model.addAttribute("products",productService.getProductIdAndName());
	}
	
	//register page
	@GetMapping("/register")
	public String showRegister(Model model)
	{
		commonUi(model);
		return "StockRegister";
	}
	
	//save stock
	@PostMapping("/save")
	public String saveStock(@ModelAttribute Stock stock,Model model)
	{
		//first get the product id
		//For particular prodcut stock is added already update the stock else create stock
		String message=null;
		Long productId=stock.getProduct().getId();
		Long id=service.getStockIdByProduct(productId);
		if(id!=null)
		{
			service.updateStock(id, stock.getCount());
			message="Stock Updated!";
		}
		else
		{	stock.setQoh(stock.getCount());
			stock.setSold(Long.valueOf(0L));
			service.saveStock(stock);
			message="Stock Added";
		}
		model.addAttribute("message",message);
		commonUi(model);
		return "StockRegister";
	}
	
}
