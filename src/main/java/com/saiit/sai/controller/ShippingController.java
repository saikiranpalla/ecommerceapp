package com.saiit.sai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.saiit.sai.entity.Shipping;
import com.saiit.sai.exception.ShippingNotFoundException;
import com.saiit.sai.service.IShippingService;

@Controller
@RequestMapping("/shipping")
public class ShippingController {
	@Autowired
	private IShippingService service;
	
	//show register page
	@GetMapping("/register")
	public String registerShipping(Model model)
	{
		return "ShippingRegister";
	}
	//save shipping
	/***
	 * on Form submit read the Object by using @ModelAttribute claname objname  
	 * call the service for saving the object into DB
	 * get the id back
	 * send the success message by using Model memory 
	 * return back to ShippingRegister.html
	 */
	@PostMapping("/save")
	public String saveShipping(@ModelAttribute Shipping shipping,Model model)
	{
		Long id=service.saveShipping(shipping);
		model.addAttribute("message","Shipping is done with id:"+id);
		return "ShippingRegister";
	}
	
	//list all shippings
	/***
	 * Get all the data from db by using service
	 * store it as list type
	 * send the data b using Model memory
	 * return back to ShippingData.html
	 * 
	 * Fetch the message from edit,delete,update pages and show the message by using @RequestParam(value="message",required = false)String message 
	 *  show the all the data
	 *  append the message to ShippingData
	 * 
	 */
	@GetMapping("/all")
	public String getAllShippings(Model model,@RequestParam(value="message",required = false)String message)
	{
		List<Shipping> list=service.getAllShippings();
		model.addAttribute("list",list);
		model.addAttribute("message",message);
		return "ShippingData";
	}
	//delete by id
	/***
	 * Get the id from request URL by using @RequestParam datatype paramname
	 * all the service to delete the object
	 * create success/failure message and send the msg back by using RedirectAttributes to redirect to all
	 * handle the exception if record is not there in db
	 * return back to all
	 */
	@GetMapping("/delete")
	public String deleteShipping(@RequestParam Long id,RedirectAttributes attributes)
	{
		try {
			service.deleteShipping(id);
			attributes.addAttribute("message","Shipping is deleted with id:"+id);
		} catch (ShippingNotFoundException e) {
			attributes.addAttribute("message",e.getMessage());
		}
		return "redirect:all";
	}
	
	//show edit
	/***
	 * Read id from request URL by using @RequestParam
	 * load the object by using findById
	 * send the object to ui by using model
	 * use Thymleaf form to read data and fill the form[ShippingEdit]
	 
	 *Fetch the data into edit page 
	 * If end user clicks on link or enter id manually
	 * If entered data is present in db
	 * >Load row as object
	 * >send data to edit page
	 * >Fill the form
	 * 
	 * Else
	 * >Redirect to all [ShippingData]
	 * >show the error message
	 */
	@GetMapping("/edit")
	public String editShipping(@RequestParam Long id,Model model)
	{
		String page=null;
		try {
			Shipping shipping=service.getOneShipping(id);
			model.addAttribute("shipping",shipping);
			page="ShippingEdit";
		} catch (ShippingNotFoundException e) {
			model.addAttribute("message",e.getMessage());
			page="redirect:all";
		}
		return page;
	}
	
	//update
	/***
	 * Take the object from edit page by using @ModelAttribute claname objname
	 * call the service to save/update the object
	 * send the message back to ui by using @RedirectAttributes
	 * after sending redirect to all [ShippingData]
	 * 
	 */
	@PostMapping("/update")
	public String updateShipping(@ModelAttribute Shipping shipping,RedirectAttributes attributes)
	{
		service.updateShipping(shipping);
		attributes.addAttribute("message","Shipping Updated");
		return "redirect:all";
	}
	
	
}
