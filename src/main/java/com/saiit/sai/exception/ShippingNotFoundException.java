package com.saiit.sai.exception;

public class ShippingNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ShippingNotFoundException() {
		super();
	}
	public ShippingNotFoundException(String message) {
		super(message);
	}

}
