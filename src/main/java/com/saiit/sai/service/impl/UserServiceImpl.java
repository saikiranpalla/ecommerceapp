package com.saiit.sai.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.saiit.sai.constants.UserStatus;
import com.saiit.sai.entity.User;
import com.saiit.sai.repository.UserRepository;
import com.saiit.sai.service.IUserService;
import com.saiit.sai.util.AppUtil;
import com.saiit.sai.util.MyMailUtil;

import jdk.javadoc.doclet.Reporter;

@Service
public class UserServiceImpl implements IUserService,UserDetailsService {

	@Autowired
	private UserRepository repo;
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	private MyMailUtil mailUtil;
	
	public Long saveUser(User user) {
		//before saving the user Encode the password
		
		//Generate password
		String pwd=AppUtil.genPwd();
		
		//encode the password
		String encodedPwd=encoder.encode(pwd);
		
		//set back to user object
		user.setPassword(encodedPwd);
		
		//set status as default
		user.setStatus(UserStatus.INACTIVE.name());
		
		Long id=repo.save(user).getId();
		
		if(id!=null) {// only on user register send email as Thread
			new Thread(new Runnable() {
		
				public void run() {
					String text = "User is Created! UserName : "+user.getEmail()
					+",Password :"+pwd
					+", With Role :"+user.getRole().name();
					mailUtil.send(user.getEmail(), "User Register", text);
				}
			}).start();	
		}
		return id;
	}

	
	public Optional<User> findByEmail(String email) {
		
		return repo.findByEmail(email);
	}

	
	public Optional<User> findByContact(String contact) {
		return repo.findByContact(contact);
	}
	
	public List<User> getAllUsers() {
		return repo.findAll();
	}
	
	//UserDetailsService is an interface it is called when we try to login, 
	//it contains one overloaded method below method
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		//load user by username(email)
		Optional<User> opt=findByEmail(username);
		
		if(!opt.isPresent())
		{
			throw new UsernameNotFoundException("User Not Exist");
		}else {
			//read user object
			User user=opt.get();
			//return spring security user object it is taking 3 par's 
			//username,password,List(RoleAsString)
			return new org.springframework.security.core.userdetails
					.User(user.getEmail(),
						  user.getPassword(),
						  Arrays.asList(
								  new SimpleGrantedAuthority(user.getRole().name())
								  )	
							);
		}
		
	}

}
