package com.saiit.sai.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saiit.sai.entity.Brand;
import com.saiit.sai.repository.BrandRepository;
import com.saiit.sai.service.IBrandService;
import com.saiit.sai.util.AppUtil;

@Service
public class BrandServiceImpl implements IBrandService {

	@Autowired
	private BrandRepository repo;
	
	@Override
	@Transactional
	public Long saveBrand(Brand brand) {
		return repo.save(brand).getId();
	}

	@Override
	@Transactional
	public void updateBrand(Brand brand) {
		repo.save(brand);
	}

	@Override
	@Transactional
	public void deleteBrand(Long id) {
		repo.deleteById(id);

	}

	@Override
	@Transactional(readOnly = true)
	public Brand getOneBrand(Long id) {
		return repo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Brand> getAllBrands() {
		
		return repo.findAll();
	}
	
	@Override
	public Map<Long, String> getBrandIdAndName() {
		List<Object[]> list=repo.getBrandIdAndName();
		return AppUtil.convertListToMapLong(list);
	}
	
	@Override
	public List<Object[]> getBrandIdAndImage() {
		return repo.getBrandIdAndImage();
	}
}
