package com.saiit.sai.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saiit.sai.entity.Product;
import com.saiit.sai.exception.ProductNotFoundException;
import com.saiit.sai.repository.ProductRepository;
import com.saiit.sai.service.IProductService;
import com.saiit.sai.util.AppUtil;

@Service
public class ProductServiceImpl implements IProductService {

	@Autowired
	private ProductRepository repo;
	
	public Long saveProduct(Product product) {
		return repo.save(product).getId();
	}

	public void deleteProduct(Long id) {
		repo.deleteById(id);

	}
	
	public void updateProduct(Product product) {
		if(product.getId() == null || !repo.existsById(product.getId()))
			throw new ProductNotFoundException("Product Not Found");
		else
		repo.save(product);
	}

	public Product getOneProduct(Long id) {
		
		return repo.findById(id).orElseThrow(()-> new ProductNotFoundException("Product Not Found"));
	}

	
	public List<Product> getAllProducts() {
		
		return repo.findAll();
	}
	
	public Map<Long, String> getProductIdAndName() {
		List<Object[]> list=repo.getProductIdAndNames();
		return AppUtil.convertListToMapLong(list);
	
	}
	@Override
	public List<Object[]> getProductsByBrand(Long brandId) {
		return repo.getProductsByBrand(brandId);
	}
	
	@Override
	public List<Object[]> getProductsByCategory(Long catId) {
		return repo.getProductsByCategory(catId);
	}
	
	@Override
	public List<Object[]> getProductByNameMatching(String input) {
		return repo.getProductByNameMatching(input);
	}

}
