package com.saiit.sai.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saiit.sai.entity.CategoryType;
import com.saiit.sai.repository.CategoryTypeRepository;
import com.saiit.sai.service.ICategoryTypeService;
import com.saiit.sai.util.AppUtil;

@Service
public class CategoryTypeServiceImpl implements ICategoryTypeService {

	@Autowired
	private CategoryTypeRepository repo;
	
	@Override
	@Transactional
	public Long saveCategoryType(CategoryType categorytype) {
		
		return repo.save(categorytype).getId();
	}

	@Override
	@Transactional
	public void updateCategoryType(CategoryType categoryType) {
		repo.save(categoryType);

	}

	@Override
	@Transactional
	public void deleteCategoryType(Long id) {
		repo.deleteById(id);

	}

	@Override
	@Transactional(readOnly = true)
	public CategoryType getOneCategoryType(Long id) {
		
		return repo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<CategoryType> getAllCategoryTypes() {
		
		return repo.findAll();
	}
	@Override
	public Map<Integer, String> getCategoryTypeIdAndName() {
		List<Object[]> list=repo.getCategoryTypeIdAndName();
		return AppUtil.convertListToMap(list);
	}

}
