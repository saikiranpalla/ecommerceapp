package com.saiit.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saiit.sai.entity.Stock;
import com.saiit.sai.repository.StockRepository;
import com.saiit.sai.service.IStockService;

@Service
public class StockServiceImpl implements IStockService {

	@Autowired
	private StockRepository repo;

	public Long saveStock(Stock stock) {

		return repo.save(stock).getId();
	}

	// Custom non select operation
	@Transactional
	public void updateStock(Long id, Long count) {
		repo.updateStock(id, count);
	}

	
	public Long getStockIdByProduct(Long productId) {
	  return repo.getStockIdByProduct(productId); 
	}
	 

	public List<Stock> getStockDetails() {

		return repo.findAll();
	}

}
