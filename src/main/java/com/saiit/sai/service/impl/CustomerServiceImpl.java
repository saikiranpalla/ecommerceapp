package com.saiit.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saiit.sai.entity.Customer;
import com.saiit.sai.repository.CustomerRepository;
import com.saiit.sai.service.ICustomerService;

@Service
public class CustomerServiceImpl implements ICustomerService {
	@Autowired
	private CustomerRepository repo;

	public Long saveCutomer(Customer customer) {
	
		return repo.save(customer).getId();
	}
	
	public List<Customer> getAllCustomers() {
		
		return repo.findAll();
	}
}
