package com.saiit.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saiit.sai.entity.Coupon;
import com.saiit.sai.repository.CouponRepository;
import com.saiit.sai.service.ICouponService;
@Service
public class CouponServiceImpl implements ICouponService {

	@Autowired
	private CouponRepository repo;
	
	@Override
	public Long saveCoupon(Coupon coupon) {
		
		return repo.save(coupon).getId();
	}

	@Override
	public void updateCoupon(Coupon coupon) {
		repo.save(coupon);
	}

	@Override
	public void deleteCoupon(Long id) {
		repo.deleteById(id);

	}

	@Override
	@Transactional(readOnly = true)
	public Coupon getOneCoupon(Long id) {
		
		return repo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Coupon> getAllCoupons() {
		
		return repo.findAll();
	}

}
