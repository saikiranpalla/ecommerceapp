package com.saiit.sai.service;

import java.util.List;
import java.util.Map;

import com.saiit.sai.entity.Category;

public interface ICategoryService {
	
	Long saveCategory(Category category);
	
	void updateCategory(Category category);
	
	void deleteCategory(Long id);
	
	Category getOneCategory(Long id);
	
	List<Category> getAllCategories();
	
	Map<Long,String> getCategoryIdAndName(String status);
	
	List<Category> getCategoryByCategoryType(Long id);
}
