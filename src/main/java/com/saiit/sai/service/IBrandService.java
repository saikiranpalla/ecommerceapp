package com.saiit.sai.service;

import java.util.List;
import java.util.Map;

import com.saiit.sai.entity.Brand;

public interface IBrandService {
	
	Long saveBrand(Brand brand);
	
	void updateBrand(Brand brand);
	
	void deleteBrand(Long id);
	
	Brand getOneBrand(Long id);
	
	List<Brand> getAllBrands();
	
	//long is for internal ID Connectivityy and string is for display
	Map<Long,String> getBrandIdAndName(); 
	
	List<Object[]>getBrandIdAndImage();

}
