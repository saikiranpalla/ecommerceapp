package com.saiit.sai.service;

import java.util.List;
import java.util.Map;

import com.saiit.sai.entity.Product;

public interface IProductService {
	
	Long saveProduct(Product product);
	void deleteProduct(Long id);
	void updateProduct(Product product);
	Product getOneProduct(Long id);
	List<Product> getAllProducts();
	Map<Long,String> getProductIdAndName();
	
	List<Object[]> getProductsByBrand(Long brandId);
	List<Object[]> getProductsByCategory(Long catId);
	
	List<Object[]> getProductByNameMatching(String input);
}	
