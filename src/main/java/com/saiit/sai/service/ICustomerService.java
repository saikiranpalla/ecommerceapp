package com.saiit.sai.service;

import java.util.List;

import com.saiit.sai.entity.Customer;

public interface ICustomerService {
	
	Long saveCutomer(Customer customer);
	List<Customer> getAllCustomers();
}
