package com.saiit.sai.service;

import java.util.List;

import com.saiit.sai.entity.Stock;

public interface IStockService {
	Long saveStock(Stock stock);
	void updateStock(Long id,Long count);
	Long getStockIdByProduct(Long productId);
	List<Stock> getStockDetails();

}
