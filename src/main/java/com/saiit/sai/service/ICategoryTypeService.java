package com.saiit.sai.service;

import java.util.List;
import java.util.Map;

import com.saiit.sai.entity.CategoryType;

public interface ICategoryTypeService {
	
	Long saveCategoryType(CategoryType categorytype);
	void updateCategoryType(CategoryType categoryType);
	void deleteCategoryType(Long id);
	
	CategoryType getOneCategoryType(Long id);
	
	List<CategoryType> getAllCategoryTypes();
	
	Map<Integer,String> getCategoryTypeIdAndName();

}
