package com.saiit.sai.service;

import java.util.List;
import java.util.Optional;

import com.saiit.sai.entity.User;

public interface IUserService {
	Long saveUser(User user);
	Optional<User> findByEmail(String email);
	List<User> getAllUsers();
	
	Optional<User> findByContact(String contact);

}
